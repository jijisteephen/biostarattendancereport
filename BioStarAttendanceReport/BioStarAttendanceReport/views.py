from django.shortcuts import render, redirect
from django.urls import reverse
from django.views.generic import TemplateView

def handler404_custom(request):
    return render(request, 'BioStartAttendanceReport/errors/404.html', status=404)


def handler500_custom(request):
    return render(request, 'BioStartAttendanceReport/errors/500.html', status=500)

class RedirectToLogin(TemplateView):

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return redirect(to=reverse('biostar_admin_dashboard'))
        return redirect(to=reverse('biostar_login'))