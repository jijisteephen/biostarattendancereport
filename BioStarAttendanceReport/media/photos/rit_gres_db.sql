-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 02, 2019 at 11:20 AM
-- Server version: 5.7.11-0ubuntu6
-- PHP Version: 7.0.22-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rit_gres_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `auth_group`
--

CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL,
  `name` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `auth_group`
--

INSERT INTO `auth_group` (`id`, `name`) VALUES
(3, 'Administrator'),
(4, 'Moderator'),
(5, 'Parent'),
(2, 'Student'),
(1, 'Teacher');

-- --------------------------------------------------------

--
-- Table structure for table `auth_group_permissions`
--

CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `auth_permission`
--

CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `auth_permission`
--

INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES
(1, 'Can add log entry', 1, 'add_logentry'),
(2, 'Can change log entry', 1, 'change_logentry'),
(3, 'Can delete log entry', 1, 'delete_logentry'),
(4, 'Can add user', 2, 'add_user'),
(5, 'Can change user', 2, 'change_user'),
(6, 'Can delete user', 2, 'delete_user'),
(7, 'Can add permission', 3, 'add_permission'),
(8, 'Can change permission', 3, 'change_permission'),
(9, 'Can delete permission', 3, 'delete_permission'),
(10, 'Can add group', 4, 'add_group'),
(11, 'Can change group', 4, 'change_group'),
(12, 'Can delete group', 4, 'delete_group'),
(13, 'Can add content type', 5, 'add_contenttype'),
(14, 'Can change content type', 5, 'change_contenttype'),
(15, 'Can delete content type', 5, 'delete_contenttype'),
(16, 'Can add session', 6, 'add_session'),
(17, 'Can change session', 6, 'change_session'),
(18, 'Can delete session', 6, 'delete_session'),
(19, 'Can add complaint registration', 7, 'add_complaintregistration'),
(20, 'Can change complaint registration', 7, 'change_complaintregistration'),
(21, 'Can delete complaint registration', 7, 'delete_complaintregistration'),
(22, 'Can add user otp', 8, 'add_userotp'),
(23, 'Can change user otp', 8, 'change_userotp'),
(24, 'Can delete user otp', 8, 'delete_userotp'),
(25, 'Can add response', 9, 'add_response'),
(26, 'Can change response', 9, 'change_response'),
(27, 'Can delete response', 9, 'delete_response'),
(28, 'Can add complaint history', 10, 'add_complainthistory'),
(29, 'Can change complaint history', 10, 'change_complainthistory'),
(30, 'Can delete complaint history', 10, 'delete_complainthistory'),
(31, 'Can add class name', 11, 'add_classname'),
(32, 'Can change class name', 11, 'change_classname'),
(33, 'Can delete class name', 11, 'delete_classname'),
(34, 'Can add complaint type', 12, 'add_complainttype'),
(35, 'Can change complaint type', 12, 'change_complainttype'),
(36, 'Can delete complaint type', 12, 'delete_complainttype'),
(37, 'Can add notification', 13, 'add_notification'),
(38, 'Can change notification', 13, 'change_notification'),
(39, 'Can delete notification', 13, 'delete_notification'),
(40, 'Can add student', 14, 'add_student'),
(41, 'Can change student', 14, 'change_student'),
(42, 'Can delete student', 14, 'delete_student'),
(43, 'Can add profile', 15, 'add_profile'),
(44, 'Can change profile', 15, 'change_profile'),
(45, 'Can delete profile', 15, 'delete_profile'),
(46, 'Can add department', 16, 'add_department'),
(47, 'Can change department', 16, 'change_department'),
(48, 'Can delete department', 16, 'delete_department'),
(49, 'Can add standard', 17, 'add_standard'),
(50, 'Can change standard', 17, 'change_standard'),
(51, 'Can delete standard', 17, 'delete_standard'),
(52, 'Can add complaint assign', 18, 'add_complaintassign'),
(53, 'Can change complaint assign', 18, 'change_complaintassign'),
(54, 'Can delete complaint assign', 18, 'delete_complaintassign'),
(55, 'Can add photo', 19, 'add_photo'),
(56, 'Can change photo', 19, 'change_photo'),
(57, 'Can delete photo', 19, 'delete_photo'),
(58, 'Can add complaint attachment', 19, 'add_complaintattachment'),
(59, 'Can change complaint attachment', 19, 'change_complaintattachment'),
(60, 'Can delete complaint attachment', 19, 'delete_complaintattachment'),
(61, 'Can add complaint attachment id', 20, 'add_complaintattachmentid'),
(62, 'Can change complaint attachment id', 20, 'change_complaintattachmentid'),
(63, 'Can delete complaint attachment id', 20, 'delete_complaintattachmentid'),
(64, 'Can add post', 21, 'add_post'),
(65, 'Can change post', 21, 'change_post'),
(66, 'Can delete post', 21, 'delete_post'),
(67, 'Can add images', 22, 'add_images'),
(68, 'Can change images', 22, 'change_images'),
(69, 'Can delete images', 22, 'delete_images');

-- --------------------------------------------------------

--
-- Table structure for table `auth_user`
--

CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(150) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `auth_user`
--

INSERT INTO `auth_user` (`id`, `password`, `last_login`, `is_superuser`, `username`, `first_name`, `last_name`, `email`, `is_staff`, `is_active`, `date_joined`) VALUES
(1, 'pbkdf2_sha256$100000$KoRE84OI9Gyi$dmTMkcgPC5uKJ+8HL2WMnB/z6CX4uDk4WRnQfq7TyJs=', '2019-03-01 04:32:40.408621', 1, 'rit', '', '', '', 1, 1, '2019-02-19 09:40:30.262997'),
(14, 'pbkdf2_sha256$100000$muB7tKg85i7u$XULOoUKFkcdBKMh7pW14XXZS+SbbqinVHwZFI6B2dqs=', '2019-02-27 10:48:46.692640', 1, 'elementz', '', '', '', 1, 1, '2019-02-27 10:48:18.671067'),
(15, 'pbkdf2_sha256$100000$0k8nf8FlbCk4$/ehpErWACZNzezp140IUde7LBuCGPw5+r2NtgS07ugA=', '2019-03-01 04:35:48.930829', 0, 'test_admin', '', '', '', 0, 1, '2019-02-27 10:49:19.000000'),
(16, 'pbkdf2_sha256$100000$RtC7hDNvyALA$UjqcdxhagpxZunYc+kS8h5DkAlJM8uVRPgTPuvESTyM=', '2019-02-27 12:11:59.566588', 0, 'test_moderator', '', '', 'test_moderator@gmail.com', 0, 1, '2019-02-27 11:57:23.153093'),
(17, 'pbkdf2_sha256$100000$9BOEZ1CwZ6C6$axtF2ayPUY3Al9OIpR054AIH0p849Y6hd85zmQp2yB4=', '2019-03-01 04:48:18.754483', 0, 'test_teacher', '', '', 'testteacher@gmail.com', 0, 1, '2019-02-27 12:29:01.116212'),
(18, 'pbkdf2_sha256$100000$5I3GuJOefHbV$cO+ga4xQ8nZlPknY4Wsp5Qb/KakjPiUbsk0DCvRS3Qk=', '2019-02-27 12:32:53.213812', 0, 'test_parent', '', '', 'test_parent@gmail.com', 0, 1, '2019-02-27 12:32:33.941899'),
(19, 'pbkdf2_sha256$100000$foMdM5GCypPd$+Y0DJbGyp0Lh/ZDSs8v917LrMdvhW36d136k6j3w2+o=', '2019-02-28 05:57:24.654653', 0, 'test_student', '', '', 'teststudent@gmail.com', 0, 1, '2019-02-27 12:35:40.638865');

-- --------------------------------------------------------

--
-- Table structure for table `auth_user_groups`
--

CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `auth_user_groups`
--

INSERT INTO `auth_user_groups` (`id`, `user_id`, `group_id`) VALUES
(1, 15, 3);

-- --------------------------------------------------------

--
-- Table structure for table `auth_user_user_permissions`
--

CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `django_admin_log`
--

CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) UNSIGNED NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `django_admin_log`
--

INSERT INTO `django_admin_log` (`id`, `action_time`, `object_id`, `object_repr`, `action_flag`, `change_message`, `content_type_id`, `user_id`) VALUES
(1, '2019-02-19 09:43:58.808471', '1', 'Teacher', 1, '[{"added": {}}]', 3, 1),
(2, '2019-02-19 09:44:05.805186', '2', 'Student', 1, '[{"added": {}}]', 3, 1),
(3, '2019-02-19 09:44:29.896007', '2', 'swetha', 1, '[{"added": {}}]', 4, 1),
(4, '2019-02-19 09:45:23.803852', '1', 'EEE', 1, '[{"added": {}}]', 9, 1),
(5, '2019-02-19 09:45:30.599164', '2', 'CSE', 1, '[{"added": {}}]', 9, 1),
(6, '2019-02-19 09:45:35.415046', '3', 'ECE', 1, '[{"added": {}}]', 9, 1),
(7, '2019-02-19 09:46:05.733658', '1', 'swetha', 1, '[{"added": {}}]', 12, 1),
(8, '2019-02-19 11:06:10.931359', '3', 'jiji', 1, '[{"added": {}}]', 4, 1),
(9, '2019-02-19 11:06:38.867140', '2', 'jiji', 1, '[{"added": {}}]', 12, 1),
(10, '2019-02-19 11:07:46.962009', '3', 'Admin', 1, '[{"added": {}}]', 3, 1),
(11, '2019-02-19 11:15:50.565375', '3', 'rit', 1, '[{"added": {}}]', 12, 1),
(12, '2019-02-19 11:16:52.253496', '3', 'Administrator', 2, '[{"changed": {"fields": ["name"]}}]', 3, 1),
(13, '2019-02-19 11:18:54.601892', '3', 'rit', 3, '', 12, 1),
(14, '2019-02-19 11:25:33.468804', '4', 'rit', 1, '[{"added": {}}]', 12, 1),
(15, '2019-02-19 12:18:37.615254', '4', 'Moderator', 1, '[{"added": {}}]', 3, 1),
(16, '2019-02-19 12:19:01.092193', '5', 'Parent', 1, '[{"added": {}}]', 3, 1),
(17, '2019-02-21 06:13:17.238229', '4', 'parent', 1, '[{"added": {}}]', 4, 1),
(18, '2019-02-21 06:13:43.683858', '5', 'Name:parent-TypeParent-Designation:EEE-StatusActive', 1, '[{"added": {}}]', 12, 1),
(19, '2019-02-22 09:05:48.437687', '8', '', 3, '', 4, 1),
(20, '2019-02-22 12:31:18.507067', '12', 'Q', 1, '[{"added": {}}]', 4, 1),
(21, '2019-02-27 10:49:19.634386', '15', 'test_admin', 1, '[{"added": {}}]', 4, 14),
(22, '2019-02-27 10:51:10.292451', '15', 'test_admin', 2, '[{"changed": {"fields": ["groups"]}}]', 4, 14),
(23, '2019-02-27 10:52:03.645367', '17', 'Profile object (17)', 3, '', 12, 14),
(24, '2019-02-27 10:52:03.694049', '16', 'Profile object (16)', 3, '', 12, 14),
(25, '2019-02-27 10:52:03.768925', '15', 'Profile object (15)', 3, '', 12, 14),
(26, '2019-02-27 10:52:03.793985', '14', 'Profile object (14)', 3, '', 12, 14),
(27, '2019-02-27 10:52:03.819545', '10', 'Profile object (10)', 3, '', 12, 14),
(28, '2019-02-27 10:52:03.844212', '9', 'Profile object (9)', 3, '', 12, 14),
(29, '2019-02-27 10:52:03.869376', '8', 'Profile object (8)', 3, '', 12, 14),
(30, '2019-02-27 10:52:03.894274', '7', 'Profile object (7)', 3, '', 12, 14),
(31, '2019-02-27 10:52:03.919424', '6', 'Profile object (6)', 3, '', 12, 14),
(32, '2019-02-27 10:52:03.944431', '5', 'Profile object (5)', 3, '', 12, 14),
(33, '2019-02-27 10:52:03.969531', '4', 'Profile object (4)', 3, '', 12, 14),
(34, '2019-02-27 10:52:03.994764', '2', 'Profile object (2)', 3, '', 12, 14),
(35, '2019-02-27 10:52:04.019758', '1', 'Profile object (1)', 3, '', 12, 14),
(36, '2019-02-27 10:56:51.727619', '18', 'Name:testadmin-TypeAdministrator-Designation:EEE-StatusActive', 1, '[{"added": {}}]', 12, 14);

-- --------------------------------------------------------

--
-- Table structure for table `django_content_type`
--

CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `django_content_type`
--

INSERT INTO `django_content_type` (`id`, `app_label`, `model`) VALUES
(1, 'admin', 'logentry'),
(4, 'auth', 'group'),
(3, 'auth', 'permission'),
(2, 'auth', 'user'),
(5, 'contenttypes', 'contenttype'),
(11, 'GrievancesSystem_app', 'classname'),
(18, 'GrievancesSystem_app', 'complaintassign'),
(19, 'GrievancesSystem_app', 'complaintattachment'),
(20, 'GrievancesSystem_app', 'complaintattachmentid'),
(10, 'GrievancesSystem_app', 'complainthistory'),
(7, 'GrievancesSystem_app', 'complaintregistration'),
(12, 'GrievancesSystem_app', 'complainttype'),
(16, 'GrievancesSystem_app', 'department'),
(22, 'GrievancesSystem_app', 'images'),
(13, 'GrievancesSystem_app', 'notification'),
(21, 'GrievancesSystem_app', 'post'),
(15, 'GrievancesSystem_app', 'profile'),
(9, 'GrievancesSystem_app', 'response'),
(17, 'GrievancesSystem_app', 'standard'),
(14, 'GrievancesSystem_app', 'student'),
(8, 'GrievancesSystem_app', 'userotp'),
(6, 'sessions', 'session');

-- --------------------------------------------------------

--
-- Table structure for table `django_migrations`
--

CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `django_migrations`
--

INSERT INTO `django_migrations` (`id`, `app`, `name`, `applied`) VALUES
(1, 'contenttypes', '0001_initial', '2019-03-01 04:25:42.754516'),
(2, 'contenttypes', '0002_remove_content_type_name', '2019-03-01 04:25:45.696028'),
(3, 'auth', '0001_initial', '2019-03-01 04:26:08.879820'),
(4, 'auth', '0002_alter_permission_name_max_length', '2019-03-01 04:26:09.152873'),
(5, 'auth', '0003_alter_user_email_max_length', '2019-03-01 04:26:09.789214'),
(6, 'auth', '0004_alter_user_username_opts', '2019-03-01 04:26:09.906928'),
(7, 'auth', '0005_alter_user_last_login_null', '2019-03-01 04:26:11.924395'),
(8, 'auth', '0006_require_contenttypes_0002', '2019-03-01 04:26:12.038856'),
(9, 'auth', '0007_alter_validators_add_error_messages', '2019-03-01 04:26:12.144972'),
(10, 'auth', '0008_alter_user_username_max_length', '2019-03-01 04:26:13.321610'),
(11, 'auth', '0009_alter_user_last_name_max_length', '2019-03-01 04:26:14.150941'),
(12, 'GrievancesSystem_app', '0001_initial', '2019-03-01 04:27:13.260655'),
(13, 'admin', '0001_initial', '2019-03-01 04:27:16.289700'),
(14, 'admin', '0002_logentry_remove_auto_add', '2019-03-01 04:27:16.387137'),
(15, 'sessions', '0001_initial', '2019-03-01 04:27:17.348360'),
(16, 'GrievancesSystem_app', '0002_photo', '2019-03-01 07:07:58.483357'),
(17, 'GrievancesSystem_app', '0003_auto_20190301_1330', '2019-03-01 08:00:23.848908'),
(18, 'GrievancesSystem_app', '0004_auto_20190301_1332', '2019-03-01 08:03:04.791520'),
(19, 'GrievancesSystem_app', '0005_complaintattachment_complaint_id', '2019-03-01 08:05:39.081624'),
(20, 'GrievancesSystem_app', '0006_auto_20190301_1417', '2019-03-01 08:47:08.568839'),
(21, 'GrievancesSystem_app', '0007_remove_complaintregistration_attachment', '2019-03-01 08:56:01.608692'),
(22, 'GrievancesSystem_app', '0008_complaintattachmentid', '2019-03-01 10:22:51.676840'),
(23, 'GrievancesSystem_app', '0009_remove_complaintattachment_complaint_id', '2019-03-01 10:23:21.480667'),
(24, 'GrievancesSystem_app', '0010_delete_complaintattachmentid', '2019-03-01 10:29:39.036098'),
(25, 'GrievancesSystem_app', '0011_auto_20190301_1600', '2019-03-01 10:30:52.304989'),
(26, 'GrievancesSystem_app', '0012_auto_20190301_1646', '2019-03-01 11:17:02.859729'),
(27, 'GrievancesSystem_app', '0013_auto_20190301_1651', '2019-03-01 11:21:21.205196');

-- --------------------------------------------------------

--
-- Table structure for table `django_session`
--

CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `django_session`
--

INSERT INTO `django_session` (`session_key`, `session_data`, `expire_date`) VALUES
('ikum866yr2xtuvcxa0r74hxid7wujzdh', 'NTk0MzhmM2FhYmMyNzgwNmFmNzU2MDM5YzgyOGQzYTM4MTZkODUzNTp7Il9hdXRoX3VzZXJfaWQiOiIxNSIsIl9hdXRoX3VzZXJfaGFzaCI6IjRhMjY0ZjlhY2MxZGQwZGRjOTY1YjI4YTRiMzQxMGRjNzQ0MThmOWQiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCJ9', '2019-03-15 04:35:48.968862'),
('km4n79k3bzwi4n24gynplkmwwos0qucy', 'ZDJlYTNmYzZiNTU3MjI3MDdkYWEyYzI2ZDJkMmQxNjU3YzBiZGFiNzp7fQ==', '2019-03-15 04:32:17.099007'),
('n2c2lz2l5l7ky2wes94nxm8b5rolu8ty', 'YmNhY2JmMjJkNjdjYjZjZGJjMDVhNTFmYzI5ZGM0Y2ZiNjU3YjNkZDp7Il9hdXRoX3VzZXJfaWQiOiIxNyIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiNTc4ZTAwM2Q2OGExOWQxZTFjNWJhNjU1YmNlM2M2NWNhODVhNjNkMCJ9', '2019-03-15 04:48:18.791839'),
('vk9ot7xp295645gxcc2glmk2la2ml288', 'ZDJlYTNmYzZiNTU3MjI3MDdkYWEyYzI2ZDJkMmQxNjU3YzBiZGFiNzp7fQ==', '2019-03-15 04:32:40.375186');

-- --------------------------------------------------------

--
-- Table structure for table `GrievancesSystem_app_classname`
--

CREATE TABLE `GrievancesSystem_app_classname` (
  `id` int(11) NOT NULL,
  `classname` varchar(250) NOT NULL,
  `department_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `GrievancesSystem_app_classname_student`
--

CREATE TABLE `GrievancesSystem_app_classname_student` (
  `id` int(11) NOT NULL,
  `classname_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `GrievancesSystem_app_classname_teacher`
--

CREATE TABLE `GrievancesSystem_app_classname_teacher` (
  `id` int(11) NOT NULL,
  `classname_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `GrievancesSystem_app_complaintassign`
--

CREATE TABLE `GrievancesSystem_app_complaintassign` (
  `id` int(11) NOT NULL,
  `complaint_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `GrievancesSystem_app_complaintassign`
--

INSERT INTO `GrievancesSystem_app_complaintassign` (`id`, `complaint_id`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `GrievancesSystem_app_complaintassign_group`
--

CREATE TABLE `GrievancesSystem_app_complaintassign_group` (
  `id` int(11) NOT NULL,
  `complaintassign_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `GrievancesSystem_app_complaintassign_group`
--

INSERT INTO `GrievancesSystem_app_complaintassign_group` (`id`, `complaintassign_id`, `group_id`) VALUES
(1, 1, 3);

-- --------------------------------------------------------

--
-- Table structure for table `GrievancesSystem_app_complaintassign_user`
--

CREATE TABLE `GrievancesSystem_app_complaintassign_user` (
  `id` int(11) NOT NULL,
  `complaintassign_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `GrievancesSystem_app_complaintassign_user`
--

INSERT INTO `GrievancesSystem_app_complaintassign_user` (`id`, `complaintassign_id`, `user_id`) VALUES
(1, 1, 14);

-- --------------------------------------------------------

--
-- Table structure for table `GrievancesSystem_app_complaintattachment`
--

CREATE TABLE `GrievancesSystem_app_complaintattachment` (
  `id` int(11) NOT NULL,
  `file` varchar(100) NOT NULL,
  `uploaded_at` datetime(6) NOT NULL,
  `complaint_id` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `GrievancesSystem_app_complaintattachment`
--

INSERT INTO `GrievancesSystem_app_complaintattachment` (`id`, `file`, `uploaded_at`, `complaint_id`) VALUES
(1, 'photos/pexels-photo-145939_4j8iAk3.jpeg', '2019-03-01 11:49:09.879311', '26'),
(2, 'photos/pexels-photo-248797_gVJfGaM.jpeg', '2019-03-01 11:49:09.918064', '26'),
(3, 'photos/pexels-photo-248797_Gvb1FAw.jpeg', '2019-03-01 11:50:38.085069', '26'),
(4, 'photos/pexels-photo-145939_b3C8UuW.jpeg', '2019-03-01 11:50:38.076776', '26');

-- --------------------------------------------------------

--
-- Table structure for table `GrievancesSystem_app_complainthistory`
--

CREATE TABLE `GrievancesSystem_app_complainthistory` (
  `id` int(11) NOT NULL,
  `tagname` varchar(250) NOT NULL,
  `tagdescription` varchar(500) NOT NULL,
  `complaintstatus` varchar(100) NOT NULL,
  `complaint_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `usertype_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `GrievancesSystem_app_complaintregistration`
--

CREATE TABLE `GrievancesSystem_app_complaintregistration` (
  `id` int(11) NOT NULL,
  `title` varchar(250) NOT NULL,
  `details` varchar(1000) NOT NULL,
  `status` varchar(100) NOT NULL,
  `datetime` datetime(6) DEFAULT NULL,
  `remark` varchar(250) NOT NULL,
  `comments` varchar(250) NOT NULL,
  `filename` varchar(1000) NOT NULL,
  `updated_on` datetime(6) NOT NULL,
  `type_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `GrievancesSystem_app_complaintregistration`
--

INSERT INTO `GrievancesSystem_app_complaintregistration` (`id`, `title`, `details`, `status`, `datetime`, `remark`, `comments`, `filename`, `updated_on`, `type_id`, `user_id`) VALUES
(1, 'test', 'test details', 'open', '2019-03-01 04:35:00.000000', 'test remark', 'test comments', 'filename1', '2019-03-01 04:37:42.666922', 1, 15),
(2, 'job 1', 'test details', 'open', '2019-03-01 07:55:00.000000', 'test remark', 'test comments', 'filename1', '2019-03-01 07:56:07.509744', 1, 15),
(3, 'sdasd', 'sdasd', 'open', '2019-03-01 07:55:00.000000', 'dsd', 'asdsad', 'asdsad', '2019-03-01 08:00:48.118674', 1, 15),
(18, 'qwrwrqwr', 'qwrqwr', 'open', '2019-03-06 00:00:00.000000', 'qwrqwr', 'qwr', 'qwrqwr', '2019-03-01 10:00:35.322795', 1, 15),
(19, 'qq', 'qq', 'open', '1899-12-04 19:12:00.000000', 'test remark', 'qq', 'qq', '2019-03-01 10:01:22.378504', 2, 15),
(20, 'eeee', 'eeeeeeeee', 'open', '2019-02-27 20:40:00.000000', 'eeeeeeeeee', 'eeee', 'eeeeeeeeee', '2019-03-01 10:04:15.697299', 1, 15),
(21, 'qwewrqwr', 'qwrqwr', 'open', '2019-03-01 10:45:00.000000', 'rwrqwr', 'qwrqwr', 'wrqwr', '2019-03-01 10:48:11.395892', 1, 15),
(22, 'sfasfs', 'fasfasf', 'open', '2019-03-01 10:45:00.000000', 'AFF', 'ffF', 'FFF', '2019-03-01 10:48:41.131277', 1, 15),
(23, 'dsg', 'sdgsdg', 'open', '2019-03-14 05:20:00.000000', 'sdgsdg', 'sdgsdg', 'sdgsdg', '2019-03-01 11:22:31.116014', 1, 15),
(24, 'sgdgsdg', 'sdgsdg', 'open', '1899-12-06 00:37:00.000000', 'sdgsdg', 'sdgsdg', 'sdgsdg', '2019-03-01 11:22:56.529745', 1, 15),
(25, 'asfasfasf', 'asfasf', 'open', '2019-03-08 11:45:00.000000', 'asfasf', 'asfasf', 'asfasf', '2019-03-01 11:47:18.038436', 1, 15),
(26, 'fdfsdf', 'sdfsdf', 'open', '2019-02-28 20:40:00.000000', 'sdfsdf', 'sdsdg', 'sdfdsf', '2019-03-01 11:50:41.786630', 1, 15),
(27, 'gg', 'gg', 'open', '2019-03-01 11:45:00.000000', 'ggg', 'ggg', 'ggg', '2019-03-01 11:54:35.946026', 2, 15);

-- --------------------------------------------------------

--
-- Table structure for table `GrievancesSystem_app_complainttype`
--

CREATE TABLE `GrievancesSystem_app_complainttype` (
  `id` int(11) NOT NULL,
  `type` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `GrievancesSystem_app_complainttype`
--

INSERT INTO `GrievancesSystem_app_complainttype` (`id`, `type`) VALUES
(1, 'complaint type 1'),
(2, 'complaint type 2');

-- --------------------------------------------------------

--
-- Table structure for table `GrievancesSystem_app_department`
--

CREATE TABLE `GrievancesSystem_app_department` (
  `id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `GrievancesSystem_app_notification`
--

CREATE TABLE `GrievancesSystem_app_notification` (
  `id` int(11) NOT NULL,
  `notification` longtext NOT NULL,
  `status` varchar(100) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `GrievancesSystem_app_profile`
--

CREATE TABLE `GrievancesSystem_app_profile` (
  `id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `designation` varchar(250) NOT NULL,
  `address` varchar(500) NOT NULL,
  `timestamp` datetime(6) NOT NULL,
  `status` varchar(100) NOT NULL,
  `department_id` int(11) DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `mobile` varchar(20) NOT NULL,
  `email` varchar(254) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `GrievancesSystem_app_profile`
--

INSERT INTO `GrievancesSystem_app_profile` (`id`, `name`, `designation`, `address`, `timestamp`, `status`, `department_id`, `type_id`, `user_id`, `mobile`, `email`) VALUES
(18, 'testadmin', 'Administrator', 'test address', '2019-02-27 10:56:51.727013', 'Active', 1, 3, 15, '8123456789', 'testadmin@gmail.com'),
(19, 'testmoderator', 'Moderator', 'test', '2019-02-27 11:57:23.591561', 'Active', 2, 4, 16, '8967765767', 'test_moderator@gmail.com'),
(20, 'testteacher', 'Teacher', 'address', '2019-02-27 12:29:01.289835', 'Active', 2, 1, 17, '7890567845', 'testteacher@gmail.com'),
(21, 'testparent', 'Engineer', 'test', '2019-02-27 12:32:34.143415', 'Active', 2, 5, 18, '8908789085', 'test_parent@gmail.com'),
(22, 'teststudent', 'Student', 'test', '2019-02-27 12:35:40.867459', 'Active', 3, 2, 19, '8967765767', 'teststudent@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `GrievancesSystem_app_response`
--

CREATE TABLE `GrievancesSystem_app_response` (
  `id` int(11) NOT NULL,
  `comment` longtext NOT NULL,
  `datetime` datetime(6) DEFAULT NULL,
  `complaint_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `GrievancesSystem_app_standard`
--

CREATE TABLE `GrievancesSystem_app_standard` (
  `id` int(11) NOT NULL,
  `standard` varchar(100) NOT NULL,
  `status` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `GrievancesSystem_app_student`
--

CREATE TABLE `GrievancesSystem_app_student` (
  `id` int(11) NOT NULL,
  `dob` date NOT NULL,
  `phone` varchar(10) NOT NULL,
  `date_of_join` date NOT NULL,
  `status` varchar(100) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `student_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `GrievancesSystem_app_userotp`
--

CREATE TABLE `GrievancesSystem_app_userotp` (
  `id` int(11) NOT NULL,
  `otp` varchar(100) NOT NULL,
  `isvalid` tinyint(1) NOT NULL,
  `datetime` datetime(6) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `auth_group`
--
ALTER TABLE `auth_group`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  ADD KEY `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` (`permission_id`);

--
-- Indexes for table `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`);

--
-- Indexes for table `auth_user`
--
ALTER TABLE `auth_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_user_groups_user_id_group_id_94350c0c_uniq` (`user_id`,`group_id`),
  ADD KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`);

--
-- Indexes for table `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_user_user_permissions_user_id_permission_id_14a6b632_uniq` (`user_id`,`permission_id`),
  ADD KEY `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` (`permission_id`);

--
-- Indexes for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `django_admin_log_content_type_id_c4bce8eb_fk_django_co` (`content_type_id`),
  ADD KEY `django_admin_log_user_id_c564eba6_fk_auth_user_id` (`user_id`);

--
-- Indexes for table `django_content_type`
--
ALTER TABLE `django_content_type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`);

--
-- Indexes for table `django_migrations`
--
ALTER TABLE `django_migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `django_session`
--
ALTER TABLE `django_session`
  ADD PRIMARY KEY (`session_key`),
  ADD KEY `django_session_expire_date_a5c62663` (`expire_date`);

--
-- Indexes for table `GrievancesSystem_app_classname`
--
ALTER TABLE `GrievancesSystem_app_classname`
  ADD PRIMARY KEY (`id`),
  ADD KEY `GrievancesSystem_app_department_id_d4d93c83_fk_Grievance` (`department_id`);

--
-- Indexes for table `GrievancesSystem_app_classname_student`
--
ALTER TABLE `GrievancesSystem_app_classname_student`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `GrievancesSystem_app_cla_classname_id_user_id_5362a8e7_uniq` (`classname_id`,`user_id`),
  ADD KEY `GrievancesSystem_app_user_id_3520bd4c_fk_auth_user` (`user_id`);

--
-- Indexes for table `GrievancesSystem_app_classname_teacher`
--
ALTER TABLE `GrievancesSystem_app_classname_teacher`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `GrievancesSystem_app_cla_classname_id_user_id_8faf3f14_uniq` (`classname_id`,`user_id`),
  ADD KEY `GrievancesSystem_app_user_id_bfeb03c8_fk_auth_user` (`user_id`);

--
-- Indexes for table `GrievancesSystem_app_complaintassign`
--
ALTER TABLE `GrievancesSystem_app_complaintassign`
  ADD PRIMARY KEY (`id`),
  ADD KEY `GrievancesSystem_app_complaint_id_8f9ca909_fk_Grievance` (`complaint_id`);

--
-- Indexes for table `GrievancesSystem_app_complaintassign_group`
--
ALTER TABLE `GrievancesSystem_app_complaintassign_group`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `GrievancesSystem_app_com_complaintassign_id_group_15967835_uniq` (`complaintassign_id`,`group_id`),
  ADD KEY `GrievancesSystem_app_group_id_417f558d_fk_auth_grou` (`group_id`);

--
-- Indexes for table `GrievancesSystem_app_complaintassign_user`
--
ALTER TABLE `GrievancesSystem_app_complaintassign_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `GrievancesSystem_app_com_complaintassign_id_user__8aaf2f41_uniq` (`complaintassign_id`,`user_id`),
  ADD KEY `GrievancesSystem_app_user_id_ee65272b_fk_auth_user` (`user_id`);

--
-- Indexes for table `GrievancesSystem_app_complaintattachment`
--
ALTER TABLE `GrievancesSystem_app_complaintattachment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `GrievancesSystem_app_complainthistory`
--
ALTER TABLE `GrievancesSystem_app_complainthistory`
  ADD PRIMARY KEY (`id`),
  ADD KEY `GrievancesSystem_app_complaint_id_90c2d532_fk_Grievance` (`complaint_id`),
  ADD KEY `GrievancesSystem_app_user_id_81739b96_fk_auth_user` (`user_id`),
  ADD KEY `GrievancesSystem_app_usertype_id_a3082f38_fk_auth_grou` (`usertype_id`);

--
-- Indexes for table `GrievancesSystem_app_complaintregistration`
--
ALTER TABLE `GrievancesSystem_app_complaintregistration`
  ADD PRIMARY KEY (`id`),
  ADD KEY `GrievancesSystem_app_type_id_eb38bddc_fk_Grievance` (`type_id`),
  ADD KEY `GrievancesSystem_app_user_id_750b15bb_fk_auth_user` (`user_id`);

--
-- Indexes for table `GrievancesSystem_app_complainttype`
--
ALTER TABLE `GrievancesSystem_app_complainttype`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `GrievancesSystem_app_department`
--
ALTER TABLE `GrievancesSystem_app_department`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `GrievancesSystem_app_notification`
--
ALTER TABLE `GrievancesSystem_app_notification`
  ADD PRIMARY KEY (`id`),
  ADD KEY `GrievancesSystem_app_user_id_28f71991_fk_auth_user` (`user_id`);

--
-- Indexes for table `GrievancesSystem_app_profile`
--
ALTER TABLE `GrievancesSystem_app_profile`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_id` (`user_id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `GrievancesSystem_app_department_id_a91f08a5_fk_Grievance` (`department_id`),
  ADD KEY `GrievancesSystem_app_profile_type_id_d5e19dd2_fk_auth_group_id` (`type_id`);

--
-- Indexes for table `GrievancesSystem_app_response`
--
ALTER TABLE `GrievancesSystem_app_response`
  ADD PRIMARY KEY (`id`),
  ADD KEY `GrievancesSystem_app_complaint_id_edddd4fc_fk_Grievance` (`complaint_id`),
  ADD KEY `GrievancesSystem_app_response_user_id_7e8af387_fk_auth_user_id` (`user_id`);

--
-- Indexes for table `GrievancesSystem_app_standard`
--
ALTER TABLE `GrievancesSystem_app_standard`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `GrievancesSystem_app_student`
--
ALTER TABLE `GrievancesSystem_app_student`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `student_id` (`student_id`),
  ADD KEY `GrievancesSystem_app_student_parent_id_503e4831_fk_auth_user_id` (`parent_id`);

--
-- Indexes for table `GrievancesSystem_app_userotp`
--
ALTER TABLE `GrievancesSystem_app_userotp`
  ADD PRIMARY KEY (`id`),
  ADD KEY `GrievancesSystem_app_userotp_user_id_4df88894_fk_auth_user_id` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `auth_group`
--
ALTER TABLE `auth_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `auth_permission`
--
ALTER TABLE `auth_permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;
--
-- AUTO_INCREMENT for table `auth_user`
--
ALTER TABLE `auth_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT for table `django_content_type`
--
ALTER TABLE `django_content_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `django_migrations`
--
ALTER TABLE `django_migrations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `GrievancesSystem_app_classname`
--
ALTER TABLE `GrievancesSystem_app_classname`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `GrievancesSystem_app_classname_student`
--
ALTER TABLE `GrievancesSystem_app_classname_student`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `GrievancesSystem_app_classname_teacher`
--
ALTER TABLE `GrievancesSystem_app_classname_teacher`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `GrievancesSystem_app_complaintassign`
--
ALTER TABLE `GrievancesSystem_app_complaintassign`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `GrievancesSystem_app_complaintassign_group`
--
ALTER TABLE `GrievancesSystem_app_complaintassign_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `GrievancesSystem_app_complaintassign_user`
--
ALTER TABLE `GrievancesSystem_app_complaintassign_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `GrievancesSystem_app_complaintattachment`
--
ALTER TABLE `GrievancesSystem_app_complaintattachment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `GrievancesSystem_app_complainthistory`
--
ALTER TABLE `GrievancesSystem_app_complainthistory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `GrievancesSystem_app_complaintregistration`
--
ALTER TABLE `GrievancesSystem_app_complaintregistration`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `GrievancesSystem_app_complainttype`
--
ALTER TABLE `GrievancesSystem_app_complainttype`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `GrievancesSystem_app_department`
--
ALTER TABLE `GrievancesSystem_app_department`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `GrievancesSystem_app_notification`
--
ALTER TABLE `GrievancesSystem_app_notification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `GrievancesSystem_app_profile`
--
ALTER TABLE `GrievancesSystem_app_profile`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `GrievancesSystem_app_response`
--
ALTER TABLE `GrievancesSystem_app_response`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `GrievancesSystem_app_standard`
--
ALTER TABLE `GrievancesSystem_app_standard`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `GrievancesSystem_app_student`
--
ALTER TABLE `GrievancesSystem_app_student`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `GrievancesSystem_app_userotp`
--
ALTER TABLE `GrievancesSystem_app_userotp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  ADD CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`);

--
-- Constraints for table `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`);

--
-- Constraints for table `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  ADD CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  ADD CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  ADD CONSTRAINT `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  ADD CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  ADD CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `GrievancesSystem_app_classname`
--
ALTER TABLE `GrievancesSystem_app_classname`
  ADD CONSTRAINT `GrievancesSystem_app_department_id_d4d93c83_fk_Grievance` FOREIGN KEY (`department_id`) REFERENCES `GrievancesSystem_app_department` (`id`);

--
-- Constraints for table `GrievancesSystem_app_classname_student`
--
ALTER TABLE `GrievancesSystem_app_classname_student`
  ADD CONSTRAINT `GrievancesSystem_app_classname_id_89c4e30b_fk_Grievance` FOREIGN KEY (`classname_id`) REFERENCES `GrievancesSystem_app_classname` (`id`),
  ADD CONSTRAINT `GrievancesSystem_app_user_id_3520bd4c_fk_auth_user` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `GrievancesSystem_app_classname_teacher`
--
ALTER TABLE `GrievancesSystem_app_classname_teacher`
  ADD CONSTRAINT `GrievancesSystem_app_classname_id_66014564_fk_Grievance` FOREIGN KEY (`classname_id`) REFERENCES `GrievancesSystem_app_classname` (`id`),
  ADD CONSTRAINT `GrievancesSystem_app_user_id_bfeb03c8_fk_auth_user` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `GrievancesSystem_app_complaintassign`
--
ALTER TABLE `GrievancesSystem_app_complaintassign`
  ADD CONSTRAINT `GrievancesSystem_app_complaint_id_8f9ca909_fk_Grievance` FOREIGN KEY (`complaint_id`) REFERENCES `GrievancesSystem_app_complaintregistration` (`id`);

--
-- Constraints for table `GrievancesSystem_app_complaintassign_group`
--
ALTER TABLE `GrievancesSystem_app_complaintassign_group`
  ADD CONSTRAINT `GrievancesSystem_app_complaintassign_id_670bbc4f_fk_Grievance` FOREIGN KEY (`complaintassign_id`) REFERENCES `GrievancesSystem_app_complaintassign` (`id`),
  ADD CONSTRAINT `GrievancesSystem_app_group_id_417f558d_fk_auth_grou` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`);

--
-- Constraints for table `GrievancesSystem_app_complaintassign_user`
--
ALTER TABLE `GrievancesSystem_app_complaintassign_user`
  ADD CONSTRAINT `GrievancesSystem_app_complaintassign_id_fd43c6a6_fk_Grievance` FOREIGN KEY (`complaintassign_id`) REFERENCES `GrievancesSystem_app_complaintassign` (`id`),
  ADD CONSTRAINT `GrievancesSystem_app_user_id_ee65272b_fk_auth_user` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `GrievancesSystem_app_complainthistory`
--
ALTER TABLE `GrievancesSystem_app_complainthistory`
  ADD CONSTRAINT `GrievancesSystem_app_complaint_id_90c2d532_fk_Grievance` FOREIGN KEY (`complaint_id`) REFERENCES `GrievancesSystem_app_complaintregistration` (`id`),
  ADD CONSTRAINT `GrievancesSystem_app_user_id_81739b96_fk_auth_user` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`),
  ADD CONSTRAINT `GrievancesSystem_app_usertype_id_a3082f38_fk_auth_grou` FOREIGN KEY (`usertype_id`) REFERENCES `auth_group` (`id`);

--
-- Constraints for table `GrievancesSystem_app_complaintregistration`
--
ALTER TABLE `GrievancesSystem_app_complaintregistration`
  ADD CONSTRAINT `GrievancesSystem_app_type_id_eb38bddc_fk_Grievance` FOREIGN KEY (`type_id`) REFERENCES `GrievancesSystem_app_complainttype` (`id`),
  ADD CONSTRAINT `GrievancesSystem_app_user_id_750b15bb_fk_auth_user` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `GrievancesSystem_app_notification`
--
ALTER TABLE `GrievancesSystem_app_notification`
  ADD CONSTRAINT `GrievancesSystem_app_user_id_28f71991_fk_auth_user` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `GrievancesSystem_app_response`
--
ALTER TABLE `GrievancesSystem_app_response`
  ADD CONSTRAINT `GrievancesSystem_app_complaint_id_edddd4fc_fk_Grievance` FOREIGN KEY (`complaint_id`) REFERENCES `GrievancesSystem_app_complaintregistration` (`id`),
  ADD CONSTRAINT `GrievancesSystem_app_response_user_id_7e8af387_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `GrievancesSystem_app_student`
--
ALTER TABLE `GrievancesSystem_app_student`
  ADD CONSTRAINT `GrievancesSystem_app_student_parent_id_503e4831_fk_auth_user_id` FOREIGN KEY (`parent_id`) REFERENCES `auth_user` (`id`),
  ADD CONSTRAINT `GrievancesSystem_app_student_student_id_5da16617_fk_auth_user_id` FOREIGN KEY (`student_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `GrievancesSystem_app_userotp`
--
ALTER TABLE `GrievancesSystem_app_userotp`
  ADD CONSTRAINT `GrievancesSystem_app_userotp_user_id_4df88894_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
