from django.urls import path

from BioStarAttendanceReportApp.views import LoginPageView, BioStarDashboard, ReportGenerationView

urlpatterns = [
    path('auth/login/', LoginPageView.as_view(), name='biostar_login'),
    path('admin/dashboard/', BioStarDashboard.as_view(), name='biostar_admin_dashboard'),
    path('report/generation/', ReportGenerationView.as_view(), name='biostar_admin_report_view'),

]