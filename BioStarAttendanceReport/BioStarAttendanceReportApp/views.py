import datetime
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.views.generic import TemplateView, ListView
from BioStarAttendanceReportApp.forms import LoginForm
from django.urls import reverse
from django.contrib.auth import logout, authenticate, login
from django.utils import timezone
from django.db import connection

class LoginPageView(TemplateView):
    template_name = "BioStarAttendanceReportApp/auth/login1.html"
    form_class = LoginForm

    def get_context_data(self, request):
        context = {}
        context['redirect_to_url'] = request.GET.get('next', reverse(
            'biostar_admin_dashboard'))  # hidden redirect url
        context['form'] = self.form_class

        return context

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated:  # logout if user already logged in
            print("logging out current user")
            logout(request)
        return render(request, self.template_name, context=self.get_context_data(request=request))

    def post(self, request, *args, **kwargs):
        # context = self.get_context_data(request=request)
        form = self.form_class(request.POST)
        post_data = request.POST
        updated_at = timezone.datetime.now()

        url_redirect = post_data.get('next', reverse('biostar_admin_dashboard'))
        if form.is_valid():
            name = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(username=name, password=password)
            if user is not None:
                login(request, user=user)  # save user to session to access inner pages
                print("user authenticated : redirecting to ", url_redirect)
                if url_redirect == reverse('biostar_admin_dashboard'):
                    url_redirect = reverse('biostar_admin_dashboard')
                return HttpResponseRedirect(url_redirect)

        context = self.get_context_data(request=request)
        context['error'] = 'Username or Password invalid!!'
        return render(request, self.template_name, context=context)


class BioStarDashboard(LoginRequiredMixin, TemplateView):
    template_name = "BioStarAttendanceReportApp/dash/admin-dash.html"

class ReportGenerationView(LoginRequiredMixin, ListView):
    # template_name = "BioStarAttendanceReportApp/report/report_generation.html"
    cursor = connection.cursor()
    cursor.execute('''SELECT  
    TBL_KEL_RDR_LOG.nDateTime As PunchDate,
            TBL_KEL_RDR_LOG.nDateTime As PunchTime,
            TBL_KEL_RDR_LOG.nEventIdn,
           TBL_KEL_RDR_LOG.nUserID,
           TBL_KEL_RDRs.RdrID, 
           TBL_KEL_RDR_LOG.nProcessed
           FROM TBL_KEL_RDR_LOG INNER JOIN TB_USER ON TBL_KEL_RDR_LOG.nUserID = TB_USER.sUserID inner join TBL_KEL_RDRS on TBL_KEL_RDR_LOG.nReaderIdn = TBL_KEL_RDRS.nReaderIdn
    WHERE (TBL_KEL_RDR_LOG.nUserID BETWEEN 1 AND 99999999) AND (TBL_KEL_RDR_LOG.nEventIdn IN (0,1,39, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 55, 58, 59, 61, 62, 98, 99, 149, 151,255))
    ''')
    row = cursor.fetchall()
    print(row)

