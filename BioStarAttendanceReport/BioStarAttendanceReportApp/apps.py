from django.apps import AppConfig


class BiostarattendancereportappConfig(AppConfig):
    name = 'BioStarAttendanceReportApp'
