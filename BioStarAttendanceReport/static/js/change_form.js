/*global showAddAnotherPopup, showRelatedObjectLookupPopup showRelatedObjectPopup updateRelatedObjectLinks*/

(function($) {
    'use strict';
    $(document).ready(function() {
        var modelName = $('#django-form-add-constants').data('modelName');
        // console.log("modelName="+modelName);
        $('body').on('click', '.add-another', function(e) {
            e.preventDefault();
            var event = $.Event('django:add-another-related');
            // console.log("event="+event);
            $(this).trigger(event);
            if (!event.isDefaultPrevented()) {

                showAddAnotherPopup(this);
            }
        });

        if (modelName) {
            $('form#' + modelName + '_form :input:visible:enabled:first').focus();
            // console.log('form#' + modelName + '_form');
        }
    });
})(django.jQuery);
